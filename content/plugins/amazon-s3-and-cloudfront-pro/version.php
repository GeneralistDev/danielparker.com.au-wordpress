<?php
$GLOBALS['aws_meta']['amazon-s3-and-cloudfront-pro']['version'] = '1.0.1';

$GLOBALS['aws_meta']['amazon-s3-and-cloudfront-pro']['supported_addon_versions'] = array(
	'amazon-s3-and-cloudfront-edd'         => '1.0',
	'amazon-s3-and-cloudfront-woocommerce' => '1.0.1',
	'amazon-s3-and-cloudfront-assets'      => '1.0.1',
);