<div class="as3cf-notice inline subtle as3cf-pro-media-notice">
	<p class="dashicons-before dashicons-admin-media">
		<?php echo $message; ?>
		<?php if ( $upload ) : ?>
			<a class="as3cf-pro-upload" href="#"><?php _e( 'Upload Now', 'as3cf-pro' ); ?></a>
		<?php endif; ?>
	</p>
</div>