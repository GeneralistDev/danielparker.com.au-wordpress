msgid ""
msgstr ""
"Project-Id-Version: Switch jQuery Version\n"
"POT-Creation-Date: 2014-03-18 18:04+0800\n"
"PO-Revision-Date: 2014-03-18 18:11+0800\n"
"Last-Translator: Arefly <eflyjason@gmail.com>\n"
"Language-Team: Arefly <eflyjason@gmail.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.4\n"
"X-Poedit-Basepath: ../\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"

#: switch-jq-version.php:8
msgid "Change your jQuery version and the CDN of jQuery just by onclick!"
msgstr "一鍵修改你的jQuery版本及所使用的CDN！"

#: options.php:13 options.php:53
msgid "Switch jQuery Version Options Page"
msgstr "切換jQuery版本"

#: options.php:13
msgid "Switch jQuery Version"
msgstr "切換jQuery版本"

#: options.php:56
msgid "General Options"
msgstr "一般設定"

#: options.php:57
#, php-format
msgid "The jQuery version of your blog is %s"
msgstr "你的部落格的jQuery版本是 %s"

#: options.php:60
msgid "jQuery Version: "
msgstr "jQuery版本："

#: options.php:63
#, php-format
msgid "(Recommend: You may delete the load of jQuery in your blog's %s)"
msgstr "（推薦：將 %s 中的jQuery刪除）"

#: options.php:67
msgid "CDN for jQuery: "
msgstr "jQuery所使用的CDN"

#: options.php:69
msgid "WordPress Default"
msgstr "WordPress默認"

#: options.php:69
msgid "Google CDN"
msgstr "Google CDN"

#: options.php:69
msgid "jQuery CDN"
msgstr "jQuery CDN"

#: options.php:69
msgid "Sina CDN"
msgstr "新浪CDN"

#: options.php:69
msgid "Customise"
msgstr "自定義"

#: options.php:70
msgid "(If you don't know what it means, leave it as default)"
msgstr "（如果你不清楚這是什麼，請保持默認）"

#: options.php:73
#, php-format
msgid "(Use %s for version number)"
msgstr "（使用 %s 替代版本號）"

#: switch-jq-version.php:48
msgid "Settings"
msgstr "設定"
